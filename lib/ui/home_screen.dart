import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_shared_assignment/ui/splash_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  String name="";
  String email="";
  String about="";
  SharedPreferences sharedPreferences;

  void initShared()async{
    sharedPreferences=await SharedPreferences.getInstance();
    setState(() {
      name=sharedPreferences.getString("name");
      about=sharedPreferences.getString("about");
      email=sharedPreferences.getString("email");
    });
  }

  void logout(){
    sharedPreferences.clear();
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => SplashScreen()),
    );
  }


  @override
  void initState() {
    initShared();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: Colors.white,
        child: _buildHomeScreen(),
      ),
    );
  }

  Widget _buildHomeScreen(){
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10,),
          Center(
            child: SizedBox(
              child: CircleAvatar(
                radius: 50,
                backgroundImage: AssetImage("assets/Unknown.jpg"),
              ),
            ),
          ),
          SizedBox(height: 5,),
          Center(child: Text(name,style: TextStyle(color: Colors.black,fontSize: 18,fontFamily: 'Roboto',fontWeight: FontWeight.bold),)),
          SizedBox(height: 5,),
          Center(child: Text(email,style: TextStyle(color:Colors.black54,fontSize: 14,fontFamily: 'Montserrat',fontWeight: FontWeight.bold),)),
          SizedBox(height: 5,),
          Center(
            child: InkWell(
              onTap: (){
                logout();
              },
              child: Container(
                width: 100,
                child: Center(
                  child: Text("Logout",style: TextStyle(color: Colors.white,fontSize: 16,fontWeight: FontWeight.bold,fontFamily: "Montserrat"),),
                ),
                padding: EdgeInsets.all(5),
                decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.all(Radius.circular(10))
                ),
              ),
            ),
          ),
          SizedBox(height: 20,),
          Text("About",style: TextStyle(color: Colors.black,fontSize: 18,fontFamily: 'Roboto',fontWeight: FontWeight.bold),),
          SizedBox(height: 10,),
          Text(about,style: TextStyle(color:Colors.black,fontSize: 14,fontFamily: 'Montserrat',fontWeight: FontWeight.bold),),
        ],
      ),
    );
  }

}
