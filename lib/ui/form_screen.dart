import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'home_screen.dart';


class FormScreen extends StatefulWidget {
  @override
  _FormScreenState createState() => _FormScreenState();
}

class _FormScreenState extends State<FormScreen> {
  String nama="";
  String email="";
  String about="";
  GlobalKey<FormState>formKey= new GlobalKey<FormState>();


  void saveToPref()async{
    ///tahap validasi
    if(formKey.currentState.validate()){
      //save nilai dari form
      formKey.currentState.save();
      SharedPreferences sharedPreferences=await SharedPreferences.getInstance();
      sharedPreferences.setString("name",nama);
      sharedPreferences.setString("email",email);
      sharedPreferences.setString("about",about);
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => HomeScreen()),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.blue,
        padding: EdgeInsets.all(10),
        child: ListView(
          children: [
            SizedBox(height: 30,),
            _buildFormLogin()
          ],
        ),
      ),
    );
  }

  Widget _buildFormLogin() {
    return Container(
      padding: EdgeInsets.all(10),
      child: Form(
        key: formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text("About App",style: TextStyle(fontSize:30,fontFamily: "Roboto",fontWeight: FontWeight.bold,color: Colors.white),),
            SizedBox(height: 20,),
            TextFormField(
              cursorColor: Colors.white,
              validator: (value){
                if(value.isEmpty){
                  return "Nama harus diisi";
                }
                return null;
              },
              onSaved: (String value) {
                nama = value;
              },
              style: TextStyle(color: Colors.white),
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                  errorStyle: TextStyle(
                      color: Colors.white
                  ),
                  fillColor: Colors.white,
                  hoverColor: Colors.white,
                  focusColor: Colors.white,
                  errorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(color: Colors.white),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    borderSide: BorderSide(color: Colors.white),
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(color: Colors.white),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(color: Colors.white),
                  ),
                  hintText: "Nama",
                  hintStyle: TextStyle(color: Colors.white)
              ),
            ),
            SizedBox(
              height: 10,
            ),
            TextFormField(
              cursorColor: Colors.white,
              validator: (value){
                if(value.isEmpty){
                  return "Email harus diisi";
                }
                return null;
              },
              onSaved: (String value) {
                email = value;
              },
              style: TextStyle(color: Colors.white),
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                  errorStyle: TextStyle(
                      color: Colors.white
                  ),
                  fillColor: Colors.white,
                  hoverColor: Colors.white,
                  focusColor: Colors.white,
                  errorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(color: Colors.white),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    borderSide: BorderSide(color: Colors.white),
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(color: Colors.white),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(color: Colors.white),
                  ),
                  hintText: "Email",
                  hintStyle: TextStyle(color: Colors.white)
              ),
            ),
            SizedBox(
              height: 10,
            ),
            TextFormField(
              maxLines: 10,
              validator: (value){
                if(value.isEmpty){
                  return "Field harus diisi";
                }
                return null;
              },
              onSaved: (String value) {
                about = value;
              },
              cursorColor: Colors.white,
              style: TextStyle(color: Colors.white),
              decoration: InputDecoration(
                errorStyle: TextStyle(
                    color: Colors.white
                ),
                fillColor: Colors.white,
                hoverColor: Colors.white,
                focusColor: Colors.white,
                errorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  borderSide: BorderSide(color: Colors.white),
                ),
                focusedErrorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  borderSide: BorderSide(color: Colors.white),
                ),
                hintText: "Tentang",
                hintStyle: TextStyle(color: Colors.white),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  borderSide: BorderSide(color: Colors.white),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  borderSide: BorderSide(color: Colors.white),
                ),
              ),//KETIKA obsecureText bernilai TRUE
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              width: double.infinity,
              child: FlatButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  color: Colors.white,
                  onPressed: () async {
                    saveToPref();
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(17.0),
                    child: Text("Masuk",
                        style: TextStyle(
                            fontSize: 18,
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontFamily: "Roboto")),
                  )),
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }

}
