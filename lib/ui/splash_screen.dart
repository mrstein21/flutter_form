import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_shared_assignment/ui/form_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'home_screen.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  SharedPreferences sharedPreferences;

  startSplashScreen()async{
    sharedPreferences = await SharedPreferences.getInstance();
    var duration = const Duration(seconds: 2);
    return Timer(duration, () {
      if(sharedPreferences.getString("email")!=null){
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => HomeScreen()),
        );
      }else{
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => FormScreen()),
        );
      }
      ///cek session
    });
  }

  @override
  void initState() {
    startSplashScreen();
    // TODO: implement initState
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.blue,
        child: Center(
          child: Text("Welcome to About App",style: TextStyle(
              fontSize: 20,
              color: Colors.white,fontWeight: FontWeight.bold,fontFamily: 'Pacifico'),),
        ),
      ),
    );
  }
}
